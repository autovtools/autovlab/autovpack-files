#!/bin/bash

# A Rosetta-Code of helper functions to make bootstrapping a deployable template easier
#   (Mostly install vm tools, salt-minion, and any packages to make a minimally useful template)

function install_third_party(){
    USAGE="install_third_party REPO_NAME REPO_ENTRY KEY_URL PACKAGES PACKAGE_MANAGER"
    if [ "$#" -ne "5" ]; then
        echo "Usage: $USAGE"
        return 1 
    fi
    REPO_NAME="$1"
    REPO_ENTRY="$2"
    KEY_URL="$3"
    PACKAGES="$4"
    PACKAGE_MANAGER="$5"

    if [ "${PACKAGE_MANAGER}" = "apt" ] || [ "${PACKAGE_MANAGER}" = "apt-get"  ]; then
        export DEBIAN_FRONTEND="noninteractive"
        wget -qO - "${KEY_URL}" | apt-key add -
        echo "${REPO_ENTRY}" > "/etc/apt/sources.list.d/${REPO_NAME}.list"
        apt_install ${PACKAGES}
    else
        echo "${PACKAGE_MANAGER} not supported"
    fi 

}

function configure_gnome(){
    # Lock screen after 1 hour
    #   (extremely useful when driving multiple VMs)
    LOCK_TIMEOUT=3600
    if [ -x "$(command -v gsetting)" ]; then
        gsettings set org.gnome.desktop.session idle-delay "${LOCK_TIMEOUT}"
    else
        echo "gsettings not found"
    fi
}

function configure_minion(){
    MINION_CONF="/etc/salt/minion"
    cp "${MINION_CONF}" "${MINION_CONF}".default
    sed -i 's/^#minion_id_caching.*$/minion_id_caching: False/' "${MINION_CONF}"
    sed -i 's/^#master:.*$/master: salt.salt/' "${MINION_CONF}"    
    sed -i "s/^#startup_states:.*$/startup_states: 'top'/" "${MINION_CONF}"

    diff "${MINION_CONF}" "${MINION_CONF}".default
    service_enable salt-minion

    # Because this is a minion, disable any master-y services
    #   (but leave them installed in case you need them later)
    service_disable salt-master salt-syndic
    
    # Since we are prepping a template that will be deployed to a different master,
    #   we need to delete any master's key we accidently picked up
    service_stop salt-minion
    rm -f "/etc/salt/pki/minion/minion_master.pub"
}

function configure_cloud_init(){
    # cloud-init's cloud.cfg may not contain all the settings we want to alter
    # So, we delete any of settings we care about from the main cloud.cfg
    # and make a custom.cfg with all the right settings
    CLOUD_CFG="/etc/cloud/cloud.cfg"
    CUSTOM_CFG="${CLOUD_CFG}.d/99-custom.cfg"
    CLOUD_SVC="/usr/lib/systemd/system/cloud-init-local.service"
    TOUCH="/usr/bin/touch"
    SLEEP="/usr/bin/sleep"
    DISABLED="/etc/cloud/cloud-init.disabled"

    # Disable helper services that may interfere with VMware
    service_disable cloud-init cloud-config cloud-final cloud-init.target

    cp "${CLOUD_CFG}" "${CLOUD_CFG}.default"
    # Delete any keys we want to set
    for KEY in disable_root ssh_pwauth disable_vmware_customization ; do
        sed -i "s/^${KEY}:.*//" "${CLOUD_CFG}"
    done

    diff "${CLOUD_CFG}.default" "${CLOUD_CFG}"

    # /var/run/vmware-imc/cust.cfg customization (what linux_options does in terraform)
    # is handled by the OVF datasource. Disable all others.
    # Also stop cloud-init from changing ssh to key-only, no-root
    cat <<EOF > "${CUSTOM_CFG}"
disable_root: 0
ssh_pwauth: 1
datasource_list: [ OVF ]
EOF
    # There is a hard-coded test for disable_vmware_customization in /etc/cloud/cloud.cfg
    # https://github.com/vmware/open-vm-tools/blob/1f128736f9975b7afa9e342ec52575f57bf63d13/open-vm-tools/libDeployPkg/linuxDeploymentUtilities.c#L51
    echo 'disable_vmware_customization: false' >> "${CLOUD_CFG}"
    echo "${CUSTOM_CFG}:"
    cat "${CUSTOM_CFG}"

    # Modify cloud-init so it self-disables if stopped (e.g. via graceful shutdown)
    # Or if the VM is powered on long enough that vmware-imc cust would have started
    # This speeds up boot times considerably and fixes https://kb.vmware.com/s/article/71264
    sed -i "\|^RemainAfterExit.*|i ExecStart=${SLEEP} 30" "${CLOUD_SVC}"
    sed -i "\|^RemainAfterExit.*|i ExecStart=${TOUCH} ${DISABLED}" "${CLOUD_SVC}"
    sed -i "\|^RemainAfterExit.*|i ExecStop=${TOUCH} ${DISABLED}" "${CLOUD_SVC}"

    # hostnamectl won't work unless dbus is started
    # https://bugzilla.redhat.com/show_bug.cgi?id=1417025
    #sed -i "s/^DefaultDependencies=no//" "${CLOUD_SVC}"

    # Fedora needs these to be set
    vmware-toolbox-cmd config set deployPkg enable-customization true
    vmware-toolbox-cmd config set deployPkg enable-custom-scripts true

    service_enable_only cloud-init-local
    # Technically, since we make cloud-init only check OVF (and not None), if OVF is not available
    #   (e.g. we were manually powered on and not deployed by terraform)
    #   this ExecStart fails, and cloud-init won't get disabled by ExecStop (which is good)
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"    
    echo "!! WARNING: This template is cloud-init enabled.                                          !!"
    echo "!! cloud-init will run on the first boot *only*                                           !!"
    echo "!! If making manual edits to the templates, you may need to run:                          !!"
    echo "!! service cloud-init stop; rm -f /etc/cloud/cloud-init.disabled; cloud-init clean --logs !!"
    echo "!! before powering off and taking a snapshot                                              !!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"    
}

function configure_docker_swarm(){
    CIL_URL="${1}"
    # Configure docker swarm on Fedora CoreOS
    SYSCONF="/etc/sysconfig/docker"
    # Disable --live-restore to work around
    # https://forums.docker.com/t/error-response-from-daemon-live-restore-daemon-configuration-is-incompatible-with-swarm-mode/28428
    # See https://castrillo.gitlab.io/figaro/post/docker-local-post/
    sed -i 's/--live-restore//' "${SYSCONF}"

    # Add an se-linux policy that allows bind-mounting /var/run/docker.sock
    if [ "${CIL_URL}" != "" ]; then
        CIL="dockersock.cil"
        download_file "${CIL_URL}" "${CIL}"
        echo "Installing selinux module ${CIL}"
        semodule -i "${CIL}"
        rm -f "${CIL}"
    fi
}

function configure_docker(){
    SYSCONF="/etc/sysconfig/docker"
    ETC_DOCKER="/etc/docker"
    DAEMON_CONF="${ETC_DOCKER}/daemon.json"
    if [ -f "${SYSCONF}" ]; then
        # Make sure sysconfig doesn't try to set default-ulimit,
        #   so we can set it in /etc/docker/daemon.json
        sudo sed -i  's/--default-ulimit nofile=[^ ]*//' "${SYSCONF}"
    fi
    
    mkdir -p "${ETC_DOCKER}"

    cat << EOF > "${DAEMON_CONF}"
{
  "default-ulimits": {
      "nofile": {
          "Name": "nofile",
          "Hard": 65536,
          "Soft": 65536
      }
  }
}
EOF
    chmod 0600 "${DAEMON_CONF}"

}

function download_file(){
    DOWNLOAD_URL="${1}"
    OUT_FILE="${2}"
    PERMS="${3}"

    if [ -x "$(command -v wget)" ]; then
        wget -q -O "${OUT_FILE}" "${DOWNLOAD_URL}"
    elif [ -x "$(command -v curl)" ]; then
        curl -s -L -o "${OUT_FILE}" "${DOWNLOAD_URL}"
    else
        echo "download_file: No supported clients found!" 1>&2
    fi

    if [ ! -f "${OUT_FILE}" ]; then
        echo "download_file: Download failed!"
    elif [ "${PERMS}" != "" ]; then
        chmod "${PERMS}" "${OUT_FILE}"
    fi
}

function add_cluster_helpers(){
    BASE_URL="${1}Linux/CoreOS/files"

    CLUSTER_NFS_DIR="/etc/cluster-nfs"
    mkdir -p "${CLUSTER_NFS_DIR}"
    for conf in haproxy.nfs.cfg cluster-nfs.exports ; do
        DEST="${CLUSTER_NFS_DIR}/${conf}"
        download_file "${BASE_URL}/${conf}" "${DEST}" 0444
    done

    for svc in cluster-nfs-init cluster-nfs cluster-nfs-proxy ; do
        FILE="${svc}.service"
        DEST="/etc/systemd/system/${FILE}"
        download_file "${BASE_URL}/${FILE}" "${DEST}" 0444
    done


    BINS="protect-container harden-node"
    BINS+=" mount-cluster-nfs umount-cluster-nfs"
    BINS+=" wait-for-cluster wait-for-gateway wait-for-ping"

    for bin in ${BINS}; do
        DEST="/usr/local/bin/${bin}"
        download_file "${BASE_URL}/${bin}" "${DEST}" 0555
    done
}

function boot_to_desktop(){
    if [ -x "$(command -v systemctl)" ]; then
        # Centos 7+
        systemctl set-default graphical.target
    else
        # Centos < 7
        # Change runlevel from 3 (multi-user w/networking ) to 5 (X11)
        sed -i 's/^id:3:/id:5:/' /etc/inittab
    fi
}

function use_sysconfig_network(){
	# Usage: use_sysconfig_network ens192
    # https://pagure.io/fedora-kickstarts/blob/master/f/fedora-cloud-base.ks#_163
	# Fixes fedora so cloud-init works
	INTERFACE="$1"
	if [ "${INTERFACE}" = "" ]; then
		INTERFACE="eth0"
	fi
	cat > /etc/sysconfig/network << EOF
NETWORKING=yes
NOZEROCONF=yes
DEVTIMEOUT=10
EOF
		
	# simple eth0 config, again not hard-coded to the build hardware
	cat > "/etc/sysconfig/network-scripts/ifcfg-${INTERFACE}" << EOF
DEVICE="${INTERFACE}"
BOOTPROTO="dhcp"
ONBOOT="yes"
TYPE="Ethernet"
PERSISTENT_DHCLIENT="yes"
EOF
    #https://pagure.io/fedora-kickstarts/blob/master/f/fedora-cloud-base.ks#_251
    # Cloud-init doesn't play nice with NetworkManager on some Fedora / CoreOS
    service_disable NetworkManager
    service_enable_only network
    # Delete any DNS settings that NetworkManager generated
    echo -n > /etc/resolv.conf
}

function service_stop(){
    if [ -x "$(command -v systemctl)" ]; then
        # Ubuntu 16+, CentOS 7+ 
        systemctl stop $@
    elif [ -x "$(command -v service)" ]; then 
        for svc in $@; do
            service $svc stop
        done
    else
        echo "ERROR: service_start does not support this machine" 1>&2 
    fi
}

function service_start(){
    if [ -x "$(command -v systemctl)" ]; then
        # Ubuntu 16+, CentOS 7+ 
        systemctl start $@
    elif [ -x "$(command -v service)" ]; then 
        for svc in $@; do
            service $svc start
        done
    else
        echo "ERROR: service_start does not support this machine" 1>&2 
    fi

}

function service_enable(){
    service_enable_only $@
    service_start $@
}

function service_enable_only(){
    if [ -x "$(command -v systemctl)" ]; then
        # Ubuntu 16+, CentOS 7+ 
        systemctl enable $@
    elif [ -x "$(command -v chkconfig)" ]; then
        # CentOS < 7
        for svc in $@ ; do
            chkconfig $svc on
        done
    else
        # UPSTART
        # https://askubuntu.com/questions/19320/how-to-enable-or-disable-services   
        for svc in $@ ; do
            rm -f /etc/init/${svc}.override
        done
    fi
}

function service_disable(){
    if [ -x "$(command -v systemctl)" ]; then
        # Ubuntu 16+, CentOS 7+ 
        for svc in $@ ; do
            systemctl disable $svc
        done
    elif [ -x "$(command -v chkconfig)" ]; then
        # CentOS < 7
        for svc in $@ ; do
            chkconfig $svc off
        done
    else
        # UPSTART
        # https://askubuntu.com/questions/19320/how-to-enable-or-disable-services   
        for svc in $@ ; do
            echo manual | tee /etc/init/${svc}.override
        done
    fi
}

function bootstrap_pip() {
    curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    sudo python get-pip.py
    rm -f get-pip.py
}

function bootstrap_salt(){
    # Old VMs can't download the bootstrap script from saltstack
    #   download our own from the file-server
    #https://repo.saltstack.com/#bootstrap
    #URL="http://bootstrap.saltstack.com"

    # Relies on packinator's template rendering
    #   converts http://x.x.x.x:yyyy/Linux/ZZZZ/... -> http://x.x.x.x:yyyy/Linux/bootstrap_salt.sh
    URL="${1}Linux/bootstrap_salt.sh"
    shift
    echo "Downloading bootstrap_salt from ${URL}"
    curl -k -L "${URL}" -o bootstrap_salt.sh
    sh bootstrap_salt.sh $@
    rm -f bootstrap_salt.sh
}

function bootstrap_salt_minion(){
    # Usage: bootstrap_salt_minion FILE_SERVER_URL
    URL="$1"
    shift
    bootstrap_salt "${URL}" -P $@
}

function bootstrap_salt_master(){
    # Usage: bootstrap_salt_master FILE_SERVER_URL
    URL="$1"
    shift
    bootstrap_salt "${URL}" -P $@
}

function set_hostname(){
    # Remove illegal hostname characters
    hostname=$(echo "$1" | sed -e 's/_/-/g' -e 's/\./-/g')

    # Change the 127.0.1.1 entry to use the new hostname
    sed -i "s/$(hostname)/${hostname}/" /etc/hosts

    # Try multiple methods for setting hostname
    echo "${hostname}" > /etc/hostname
    if [ -x "$(command -v hostname)" ]; then
        hostname "${hostname}"
    fi 
    if [ -x "$(command -v hostnamectl)" ]; then
        hostnamectl set-hostname "${hostname}"
    fi
    if [ -f "/etc/sysconfig/network" ]; then
        # CentOS will use this on next boot
        # set HOSTNAME="MY_HOSTNAME"
        # sed < CentOS 6 does not have -E 
        sed -i 's/^HOSTNAME=.*/HOSTNAME="'${hostname}'"/' /etc/sysconfig/network
    fi
}

##### BEGIN: APT #####

function apt_install(){
    APT="apt-get"
    if [ -x "$(command -v apt)" ]; then
        # System is newer
        APT="apt"
    fi
    echo "apt_install: $@" 1>&2
    DEBIAN_FRONTEND="noninteractive" $APT update
    DEBIAN_FRONTEND="noninteractive" $APT install -y $@
}

function zero_free_space(){
    # Unmap unused space so hypervisore can reclaim
    # https://storagehub.vmware.com/t/vsan-space-efficiency-technologies/unmap-trim-space-reclamation-on-vsan-1/
    
    # Enable UNMAP on vSAN with:
    # Get-Cluster -name MyCluster | Set-VsanClusterConfiguration GuestTrimUnmap:$true
    fstrim /
}

function apt_clean(){
    DEBIAN_FRONTEND="noninteractive" $APT autoremove -y
    DEBIAN_FRONTEND="noninteractive" $APT autoclean -y
    DEBIAN_FRONTEND="noninteractive" $APT clean -y
    zero_free_space
}

function apt_full_upgrade(){
    APT="apt-get"
    UPGRADE="dist-upgrade -y"
    if [ -x "$(command -v apt)" ]; then
        # System is newer
        APT="apt"
        UPGRADE="full-upgrade -y"
    fi
    $APT update
    DEBIAN_FRONTEND="noninteractive" $APT $UPGRADE $@
    DEBIAN_FRONTEND="noninteractive" $APT autoremove -y
}

##### END: APT #####

##### BEGIN: YUM #####

function yum_install(){
    yum install -y $@
}

function yum_groupinstall(){
    yum groupinstall -y "$@"
}

function yum_full_upgrade(){
    yum upgrade -y yum kernel
    yum upgrade -y
    yum autoremove -y
}

function yum_clean(){
    yum autoremove -y
    yum clean all -y
    zero_free_space
}

##### END: YUM #####

##### BEGIN: DNF #####

function dnf_install(){
    dnf install -y $@
}

function dnf_groupinstall(){
    dnf groupinstall -y "$@"
}

function dnf_full_upgrade(){
    dnf upgrade -y --refresh
    dnf autoremove -y
}

function dnf_clean(){
    dnf autoremove -y
    dnf clean all -y
    zero_free_space
}

##### END: DNF #####

##### BEGIN: ZYPPER #####

function zypper_install(){
    zypper -n install --force $@
}

function zypper_install_pattern(){
    zypper -n install -t pattern $@
}

function zypper_full_upgrade(){
    zypper -n dup
}

function zypper_refresh(){
    zypper -n --gpg-auto-import-keys refresh
}

##### END: ZYPPER #####
