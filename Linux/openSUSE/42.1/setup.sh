#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

if [[ "${OS_VARIANT}" =~ .*Prod ]]; then
    # This template is for running infrastructure, apply updates
    zypper_full_upgrade
fi

set_hostname "${HOSTNAME}" 

SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"
zypper_install ${SALT_PACKAGES}
configure_minion

# Since openSUSE x86 images aren't currently supported,
#   just install 32bit support on all the x64
zypper_install_pattern "32bit"

if [[ "${OS_VARIANT}" =~ ^Dev ]] || [[ "${OS_VARIANT}" =~ ^Desktop ]]; then
    # Install a GUI (opensuse defaults to KDE)
    zypper_install_pattern "kde_plasma"
    boot_to_desktop
fi
if [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    zypper_install_pattern "devel_basis"
    zypper_install_pattern "devel_C_C++"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    zypper_install ${EXTRA_PACKAGES}
fi

exit 0
