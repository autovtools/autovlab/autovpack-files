#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift

shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

set_hostname "${HOSTNAME}" 
SALT_RPM='https://repo.saltstack.com/yum/redhat/salt-repo-latest.el6.noarch.rpm'
SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"

yum_install --nogpgcheck "${SALT_RPM}"
yum clean expire-cache

yum_install ${SALT_PACKAGES}
configure_minion

# Install cloud-init for GuestOS customization
# We need to upgrade python for cloud-init to install
yum_install "python"
yum_install "cloud-init"
configure_cloud_init

if [ "${OS_VARIANT}" = "Desktop" ] || [ "${OS_VARIANT}" = "Dev" ]; then
    # Install + enable the GUI
    EXTRA_PACKAGES+=" libXfont open-vm-tools-desktop xorg*vm*"
    yum_groupinstall "GNOME Desktop"
    boot_to_desktop
fi
if [ "${OS_VARIANT}" = "Dev" ]; then
    yum_groupinstall "Development Tools"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    yum_install ${EXTRA_PACKAGES}
fi

configure_gnome
yum_clean
exit 0
