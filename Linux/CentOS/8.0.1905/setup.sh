#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done


if [[ "${OS_VARIANT}" =~ .*Prod ]]; then
    # This template is for running infrastructure, apply updates
    yum_full_upgrade
fi

set_hostname "${HOSTNAME}" 
SALT_RPM='https://repo.saltstack.com/py3/redhat/salt-py3-repo-latest.el8.noarch.rpm'
SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"

yum_install --nogpgcheck "${SALT_RPM}"
# Yum is too old to do modern TLS
sed -i -E 's/(baseurl=http)s/\1/' /etc/yum.repos.d/salt-py3-latest.repo
yum clean expire-cache

yum_install ${SALT_PACKAGES}
configure_minion

# Install cloud-init for GuestOS customization
yum_install "cloud-init"
configure_cloud_init

if [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    yum_groupinstall "Development Tools"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    yum_install ${EXTRA_PACKAGES}
fi

configure_gnome
yum_clean
exit 0
