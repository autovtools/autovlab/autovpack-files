#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done


if [[ "${OS_VARIANT}" =~ .*Prod ]]; then
    # This template is for running infrastructure, apply updates
    dnf_full_upgrade
fi

set_hostname "${HOSTNAME}" 
SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"

dnf_install ${SALT_PACKAGES}
configure_minion

dnf_install network-scripts cloud-init
configure_cloud_init

EXTRA_PACKAGES=""
if [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    dnf_groupinstall "Development Tools"
    EXTRA_PACKAGES+=" vim tmux"
fi

configure_gnome

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    dnf_install ${EXTRA_PACKAGES}
fi

use_sysconfig_network "ens192"

dnf_clean

exit 0

