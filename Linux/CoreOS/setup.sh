#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift
helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

# CoreOS only:
# /usr is read-only
mount -o remount,rw /usr

# Stop + Disable the service that performs automatic updates
# (Prevents automatic reboots)
service_disable zincati.service

# Ultra-paranoid: Stop + Disable + Delete user for the telemetry service
#   (reporting was already disabled via ignition)
service_disable fedora-coreos-pinger
userdel fedora-coreos-pinger

# CoreOS uses release streams
# https://docs.fedoraproject.org/en-US/fedora-coreos/update-streams/#_switching_to_a_different_stream

# Because https://github.com/coreos/rpm-ostree/issues/415
#   makes installing packages on 'stable' very unreliable,
#   only testing (default) or next
STREAM=""
if [[ "${OS_VARIANT}" =~ .*Next ]]; then
    STREAM="next"
fi 
if [ "${STREAM}" != "" ]; then
    # https://github.com/projectatomic/rpm-ostree/issues/415
    rpm-ostree cleanup -m
    rpm-ostree upgrade
    echo "Rebasing to stream ${STREAM}"
    # Perform the rebase to a different stream
    rpm-ostree rebase "fedora/x86_64/coreos/${STREAM}"
fi

set_hostname "${HOSTNAME}" 
# Any packages that need to be configured need to be installed by ignition
# (rpm-ostree doesn't actually apply changes until reboot)

#SALT_PACKAGES="salt-minion"
# rpm-ostree install ${SALT_PACKAGES}
#configure_minion

# rpm-ostree install "cloud-init"
configure_cloud_init

# CoreOS only:
# cloud-init searches for libdeployPkgPlugin.so in /usr/lib
# CoreOS uses /usr/lib64
ln -s ../lib64/open-vm-tools /usr/lib/open-vm-tools
# Patch
# https://github.com/canonical/cloud-init/blob/master/cloudinit/sources/helpers/vmware/imc/config_nic.py#L165
# That was fixed in
# https://github.com/canonical/cloud-init/commit/ce33e423cde806a0590fec635778d62836e1bd37#diff-0fcdb168e3d08b477daaed986dbdd8c8
# (Fixes unpack expected 2, got 1)
sed -i 's/return \[subnet\]/return (\[subnet\], route_list)/' $(find /usr/lib/ -name "config_nic.py")


if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    rpm-ostree install ${EXTRA_PACKAGES}
fi

# Set up docker
configure_docker

# Download dockersock.cil, an selinux policy
#   that allows mounting /var/run/docker.sock in a container
# It's a simple text file, but may need to be regenerated in the future
# Use newest Fedora (to match CoreOS)
# 
# #!/bin/bash
# git clone https://github.com/dpw/selinux-dockersock/
# cd selinux-dockersock
# make dockersock.pp
# /usr/libexec/selinux/hll/pp < dockersock.pp > dockersock.cil

CIL_URL="${FILE_SERVER}Linux/CoreOS/files/dockersock.cil"
configure_docker_swarm "${CIL_URL}"

service_enable_only docker

# Download some helper scripts and services to help set up a cluster
add_cluster_helpers "${FILE_SERVER}"

# Disable cloud-init after next boot
service_enable_only disable-cloud-init

# Install python packages the guestinfo customizer needs
pip3 install paramiko

use_sysconfig_network "ens192"

# Delete any DNS settings that NetworkManager generated
echo -n > /etc/resolv.conf

exit 0

