#!/bin/sh

# Stripped down version of Linux/helpers.sh
#   with tweaks to work on BSD

configure_minion(){
    MINION_CONF="/usr/local/etc/salt/minion"
    cp "${MINION_CONF}.sample" "${MINION_CONF}"
    # On BSD, -i requires a backup extension
    sed -i.bak 's/^#minion_id_caching.*$/minion_id_caching: False/' "${MINION_CONF}"
    sed -i.bak 's/^#master:.*$/master: salt.salt/' "${MINION_CONF}"    
    sed -i.bak "s/^#startup_states:.*$/startup_states: 'top'/" "${MINION_CONF}"
    # Delete the tmp backups
    rm -f "${MINION_CONF}.bak"

    diff "${MINION_CONF}" "${MINION_CONF}".sample

    # On BSD, installation shouldn't enable by default
    #   Also, on BSD salt_minion instead of salt-minion
    service_enable salt_minion
    # Just in case, make sure we didn't get a pick up a master key
    rm -f "/usr/local/etc/salt/pki/minion/minion_master.pub"
}
configure_minion_pfsense(){
    # pfSense is special FreeBSD
    configure_minion
    
    # pfSense ignores rc.conf; only cares about rc.conf.local
    echo 'salt_minion_enable="YES"' >> /usr/local/etc/rc.conf.local
    # Also, pfSense ignores any rc.d script that doesn't end in .sh
    cp /usr/local/etc/rc.d/salt_minion /usr/local/etc/rc.d/salt_minion.sh
}
_sysrc(){
    VAL="$1"
    shift
    # FreeBSD
    for svc in $@ ; do
        sysrc "${svc}_enable=\"${VAL}\""
    done
}
service_enable(){
    _sysrc YES $@
}

service_disable(){
    _sysrc NO $@
}

set_hostname(){
    # Remove illegal hostname characters
    hostname=$(echo "$1" | sed -e 's/_/-/g' -e 's/\./-/g')

    # Try multiple methods for setting hostname
    echo "${hostname}" > /etc/hostname
    if [ -x "$(command -v hostname)" ]; then
        hostname "${hostname}"
    fi 
    if [ -x "$(command -v hostnamectl)" ]; then
        hostnamectl set-hostname "${hostname}"
    fi
    if [ -x "$(command -v sysrc)" ]; then
        # FreeBSD: update /etc/rc.conf
        sysrc "hostname=${hostname}"
    fi
}
set_hostname_pfsense(){
    CONF="/cf/conf/config.xml"
    hostname=$(echo "$1" | sed -e 's/_/-/g' -e 's/\./-/g')
    sed -i.bak "s|<hostname>.*</hostname>|<hostname>${hostname}</hostname>|" "${CONF}"
    rm -f "${CONF}.bak"

    # When viconfig exits, pfSense reloads config.xml
    #   (viconfig exits immediately when input is not a termnial)
    echo "bump" | viconfig
}


##### BEGIN: PKG #####

pkg_noninteractive () {
    export ASSUME_ALWAYS_YES="yes"
}

pkg_install(){
    pkg_noninteractive 
    pkg-static install $@
}

pkg_full_upgrade(){
    pkg_noninteractive 
    pkg-static update
    pkg-static upgrade
}

##### END: PKG #####
