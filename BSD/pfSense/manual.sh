#!/bin/sh
# to be run in the manual shell post-installation
#   (inside the chroot, but before first boot)

#dhclinet vmx0
#fetch -o /tmp/manual.sh http://X.X.X.X:YYYY/BSD/pfSense/manual.sh
#sh /tmp/manual.sh

# PHP one-liner to properly set pfSense web password
#   (root password is reverted on first boot otherwise)
SET_PASS='<?php require("/etc/inc/config.inc"); require("/etc/inc/auth.inc"); $user =& getUserEntryByUID(0); local_user_set_password($user, "{{vm_password}}"); local_user_set($user); write_config("automation password set"); ?>'
# PHP one-liner to make pfSense upgrade our config.xml format
UPGRADE_CONFIG='<?php require("/etc/inc/globals.inc"); require("/etc/inc/config.inc"); convert_config(); ?>'

ETC="/usr/local/etc"
SUDOERS="${ETC}/sudoers"
LOCAL="${ETC}/rc.conf.local"
GUESTD="${ETC}/rc.d/vmware-guestd"
KMOD="${ETC}/rc.d/vmware-kmod"
SUDOERS="${ETC}/sudoers"
PFSENSE_UPGRADE="/usr/local/libexec/pfSense-upgrade"
PHP="/usr/local/bin/php-cgi"
AGENT_SRC="/tmp/guestinfo_agent.py"
AGENT_DEST="${ETC}/rc.d/guestinfo_agent.py"
AGENT_SVC="${ETC}/rc.d/guestinfo_agent.sh"

export ASSUME_ALWAYS_YES=yes
pkg update
pkg install open-vm-tools-nox11
pkg install sudo

# Enable wheel and admins as sudoers
echo '%wheel ALL=(ALL) ALL' >> "${SUDOERS}"
echo '%admins ALL=(ALL) ALL' >> "${SUDOERS}"

# Enable open-vm-tools and sshd
echo 'sshd_enable="YES"' > "${LOCAL}"
echo 'vmware_kmod_enable="YES"' >> "${LOCAL}" 
echo 'vmware_guestd_enable="YES"' >> "${LOCAL}" 
cp "${KMOD}" "${KMOD}.sh"
cp "${GUESTD}" "${GUESTD}.sh"

# If needed, install the customization agent
if [ -e "${AGENT_SRC}" ]; then
    mv "${AGENT_SRC}" "${AGENT_DEST}"
    # Only .sh scripts get executed
    # Make a .sh wrapper around our python script
    echo '#!/bin/sh' > "${AGENT_SVC}"
    echo "${AGENT_DEST}" >> "${AGENT_SVC}"
    chmod +x "${AGENT_SVC}"
    chmod +x "${AGENT_DEST}"
    echo 'guestinfo_agent_enable="YES"' >> "${LOCAL}" 
fi

# Upgrade the config file, or sshd might be blocked on startup
# See /etc/rc.bootup for how pfSense does the conversion
# Just load the variables / functions we need and call the right php function
echo "'"${UPGRADE_CONFIG}"'" | "${PHP}" -a

# Set passwords, make packer an admin
# For some reason, the admins group doesn't exist yet
pw groupadd admins

# We are in sh, so no history file
echo '{{vm_password}}' | pw useradd '{{vm_user}}' -h 0 -m -G wheel,admins
echo '{{vm_password}}' | pw usermod root -h 0

echo "'"${SET_PASS}"'" | "${PHP}" -a

# Disable pfSense-upgrade, so it doesn't break the pkg repos
mv "${PFSENSE_UPGRADE}" "${PFSENSE_UPGRADE}.disabled"

# 2.4.0 has boot bug
# https://www.thegeekpub.com/14848/pfsense-hangs-at-booting/
if [ "$(cat /etc/version)" = "2.4.0-RELEASE" ]; then
    # Use serial console (instead of VGA)
    echo 'kern.vty="sc"' >> /boot/loader.conf
fi

# Delete self (we have creds) and reboot
rm -f "$0"
reboot
