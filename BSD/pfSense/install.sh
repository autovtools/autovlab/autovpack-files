#!/bin/sh
## Recovery mode
#dhclient vmx0
#mkdir -p /tmp/mnt/cf/conf/
#fetch -o /tmp/mnt/cf/conf/config.xml http://X.X.X.X:YYYY/BSD/pfSense/config.xml
#fetch -o /tmp/install.sh http://X.X.X.X:YYYY/BSD/pfSense/install.sh
#sh /tmp/install.sh
#/scripts/lua_installer

# PHP one-liner to properly set pfSense web password
#   (root password is reverted on first boot otherwise)
#   (Escaping is hard for this one, bc all the heredoc magic)
SET_PASS='<?php require("/etc/inc/config.inc"); require("/etc/inc/auth.inc"); \$user =& getUserEntryByUID(0); local_user_set_password(\$user, "{{vm_password}}"); local_user_set(\$user); write_config("automation password set"); ?>'
# PHP one-liner to make pfSense upgrade our config.xml format

AFTER="/usr/local/bin/after_installation_routines.sh"
LOCAL="/usr/local/etc/rc.conf.local"
SSHD="/usr/local/etc/rc.d/sshd"
GUESTD="/usr/local/etc/rc.d/vmware-guestd"
KMOD="/usr/local/etc/rc.d/vmware-kmod"
SUDOERS="/usr/local/etc/sudoers"
PACKER="/usr/local/etc/rc.d/packer.sh"
PKG_CONF="/usr/local/etc/pkg.conf"
PFSENSE_CONF="/usr/local/etc/pkg/repos/pfSense.conf"
PFSENSE_UPGRADE="/usr/local/libexec/pfSense-upgrade"
PHP="/usr/local/bin/php-cgi"
AGENT_SRC="/tmp/guestinfo_agent.py"
AGENT_DEST="/usr/local/etc/rc.d/guestinfo_agent.py"
AGENT_SVC="/usr/local/etc/rc.d/guestinfo_agent.sh"

sed -i.bak 's|^/bin/sync|#/bin/sync|' "${AFTER}"

# 1. Append a script to /usr/local/bin/after_installation_routines.sh
#    (which is executed when the new system in mounted at /mnt)
# 2. That script creates a run-once script at /usr/local/etc/rc.d/packer.sh
#    that runs on the first boot of the new system
# Note: All variables are interpolated / replaced *now*, meaning their values
#       are "baked in" using their current values (when install.sh is executed)
cat <<EOF >> "${AFTER}"
# Run by after_installation_routines.sh (during install, before chroot)
# At the end of the install, inject a self-deleting script into the new system

cat <<EOD > "/mnt${PACKER}"
#!/bin/sh
# Run by the new system, started by /usr/local/etc/rc.conf.local on first boot
#   install packages needed for packer to connect
#   set packer passwords

# Make sure networking is up
sleep 10
export ASSUME_ALWAYS_YES=yes
pkg update
pkg install open-vm-tools-nox11
pkg install sudo

# Enable wheel and admins as sudoers
echo '%wheel ALL=(ALL) ALL' >> "${SUDOERS}"
echo '%admins ALL=(ALL) ALL' >> "${SUDOERS}"

# Enable open-vm-tools and sshd
echo 'sshd_enable="YES"' > "${LOCAL}"
echo 'vmware_kmod_enable="YES"' >> "${LOCAL}" 
echo 'vmware_guestd_enable="YES"' >> "${LOCAL}" 
mv "${SSHD}" "${SSHD}.sh"
mv "${KMOD}" "${KMOD}.sh"
mv "${GUESTD}" "${GUESTD}.sh"

# If needed, install the customization agent
if [ -e "${AGENT_DEST}" ]; then
    # Only .sh scripts get executed
    # Make a .sh wrapper around our python script
    echo '#!/bin/sh' > "${AGENT_SVC}"
    echo "${AGENT_DEST}" >> "${AGENT_SVC}"
    chmod +x "${AGENT_SVC}"
    chmod +x "${AGENT_DEST}"
    echo 'guestinfo_agent_enable="YES"' >> "${LOCAL}" 
fi

# Set passwords, make packer an admin
echo '{{vm_password}}' | pw useradd '{{vm_user}}' -h 0 -m -G wheel,admins
echo '{{vm_password}}' | pw usermod root -h 0

# We are in sh, so no history file to worry about
echo '${SET_PASS}' | "${PHP}" -a

# Self-delete and reboot
rm -f "${PACKER}"
reboot

EOD

# Still running in after_installation_routines.sh, when the new system is mounted at /mnt/
chmod +x "/mnt${PACKER}"
echo 'packer_enable="YES"' > "/mnt${LOCAL}"
# Disable pfSense-upgrade, so it doesn't break the pkg repos
mv /mnt${PFSENSE_UPGRADE} /mnt${PFSENSE_UPGRADE}.disabled

# Copy the agent to the new system, if it exists
if [ -e ${AGENT_SRC} ]; then
    cp "${AGENT_SRC}" "/mnt${AGENT_DEST}"
fi
#Re-add the disk sync at the end of afterXXX
/bin/sync
EOF
